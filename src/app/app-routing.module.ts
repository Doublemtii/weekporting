import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ReportingsComponent } from './pages/reportings/reportings.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { TokenpasteComponent } from './pages/tokenpaste/tokenpaste.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/tKenfIl', 
    pathMatch: 'full'
  },
  {
    path: 'tKenfIl',
    component: TokenpasteComponent
  },
  {
    path : 'home',
    component : HomeComponent,
  },
  {
    path : 'reportings',
    component : ReportingsComponent,
  },
  { 
    path: '**', 
    component: PageNotFoundComponent 
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
