import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
// Flexbox and CSS Grid (both)
import {FlexLayoutModule} from '@angular/flex-layout';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { ReportingsComponent } from './pages/reportings/reportings.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { HttpClientModule } from '@angular/common/http';
import { ProjectComponent } from './pages/project/project.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HighchartsChartModule } from 'highcharts-angular';
import { HeaderComponent } from './pages/header/header.component';
import { FooterComponent } from './pages/footer/footer.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { SigninComponent } from './pages/signin/signin.component';
import { TokenpasteComponent } from './pages/tokenpaste/tokenpaste.component';
import { ToastrModule } from 'ngx-toastr';
import { NgxInactivityDetectorModule } from 'ngx-inactivity-detector';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ReportingsComponent,
    PageNotFoundComponent,
    ProjectComponent,
    HeaderComponent,
    FooterComponent,
    SigninComponent,
    TokenpasteComponent
  ],
  imports: [
    AppRoutingModule,
    FlexLayoutModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule, 
    BrowserAnimationsModule,
    HighchartsChartModule,
    NgxPaginationModule,
    NgxInactivityDetectorModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
