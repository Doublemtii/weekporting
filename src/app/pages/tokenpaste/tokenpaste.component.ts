import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from '../../shared.service';

@Component({
  selector: 'app-tokenpaste',
  templateUrl: './tokenpaste.component.html',
  styleUrls: ['./tokenpaste.component.scss']
})
export class TokenpasteComponent implements OnInit {
  searchForm: FormGroup = new FormGroup({});
  isLoading : boolean = false;

  constructor(private router: Router, private http: HttpClient,public fb: FormBuilder, private toastr : ToastrService,
    private sharedService: SharedService) { }

  ngOnInit(): void {
    this.tokenFormat();
  }

  tokenFormat() {
    this.searchForm = this.fb.group({
      token: ['', [Validators.required]]
    })
  }

  onSubmit() {
    this.isLoading = true;
    const { token } = this.searchForm.value;
    let url = `https://gitlab.com/api/v4/projects?access_token=${token}&visibility=private&owned=false`;
    this.http.get(url)
    .subscribe(response => {
      this.isLoading = false;
      this.toastr.success("Welcome !");
      setTimeout(() => {
        localStorage.setItem('gItaOkn', this.sharedService.encrypt(token));
        this.router.navigate(['/home']);
      }, 2000);
    }, err => {
      this.isLoading = false;
      if(err.status === 401) {
        this.toastr.error("Invalid access token !");
      } else {
        this.toastr.error("Unknown error !");
      }
      this.searchForm.reset();
    });
  }

}
