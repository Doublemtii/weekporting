import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TokenpasteComponent } from './tokenpaste.component';

describe('TokenpasteComponent', () => {
  let component: TokenpasteComponent;
  let fixture: ComponentFixture<TokenpasteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TokenpasteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TokenpasteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
