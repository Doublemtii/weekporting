import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from '../../shared.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  projects = Array<any>();
  p: number = 1;
  isLoading : boolean = true;
  token = localStorage.getItem('gItaOkn');
  @Input() id: string;
  @Input() maxSize: number;
  @Output() pageChange: EventEmitter<number>;
  @Output() pageBoundsCorrection: EventEmitter<number>;
  projectsFiltered = Array<any>();
  imgPaths = [
    'assets/images/grid/img1.png',
    'assets/images/grid/img2.png',
    'assets/images/grid/img3.png',
    'assets/images/grid/img4.png',
    'assets/images/grid/img5.png',
    'assets/images/grid/img6.png',
    'assets/images/grid/img7.png',
    'assets/images/grid/img8.png',
    'assets/images/grid/img9.png',
    'assets/images/grid/img10.png',
    'assets/images/grid/img11.png',
    'assets/images/grid/img12.png',
    'assets/images/grid/img13.png',
    'assets/images/grid/img14.png',
    'assets/images/grid/img15.png'
  ];
  constructor(private http: HttpClient, private toastr : ToastrService, private router: Router, private sharedService : SharedService) { }

  ngOnInit(): void {
    if(this.token === undefined || this.token === null) {
      this.toastr.warning("No access token given !");
      setTimeout(() => {
        this.router.navigate(['/']);
      }, 1000);
    } else {
      this.token = this.sharedService.decrypt(localStorage.getItem('gItaOkn'));
      this.getProjects();
    }
  }

  getProjects() {
    
    let url = `https://gitlab.com/api/v4/projects?access_token=${this.token}&visibility=private&owned=false`;

    this.http.get(url)
      .subscribe(response => {
        this.isLoading = false;
        this.projects.push(response);
        this.projects = this.projects[0];
        this.projects.map(project => {
          project.bgCover = this.imgPaths[Math.floor(Math.random() * this.imgPaths.length)];
        });
        this.projectsFiltered = this.projects;

        this.toastr.info(this.projects.length + " project(s) were found !");
      },
        error => {
          if(error.status === 401) {
            this.toastr.error("Invalid access token !");
            localStorage.removeItem('gItaOkn');
            this.router.navigate(['/']);
          } else {
            this.toastr.error("Unknown error !");
          }
        })
  }

  filter(event: any) {
    this.projectsFiltered = this.projects;
    if (event.target.value.trim().length >= 0) {
      this.projectsFiltered = this.projectsFiltered.filter(project => project.name_with_namespace.toLowerCase().includes(event.target.value.toLowerCase()) || project.name.toLowerCase().includes(event.target.value.toLowerCase()));
    }
  }
}
