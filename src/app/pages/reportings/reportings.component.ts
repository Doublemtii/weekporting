import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as Highcharts from 'highcharts';
import { SharedService } from 'src/app/shared.service';
require('highcharts/modules/exporting')(Highcharts);
require('highcharts/modules/export-data')(Highcharts);
require('highcharts/modules/annotations')(Highcharts);
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';


declare var require: any;
@Component({
  selector: 'app-reportings',
  templateUrl: './reportings.component.html',
  styleUrls: ['./reportings.component.scss']
})
export class ReportingsComponent implements OnInit {
  branches: Array<any> = [];
  projectId: string = "";
  projectName: string = "";
  commits: Array<any> = [];
  urls: Array<string> = [];
  searchForm: FormGroup = new FormGroup({});
  results: any[] = [];
  categories: Array<string> = [];
  series: Array<any> = [];
  highcharts: typeof Highcharts = Highcharts;;
  chartOptions: any = {};
  showChart: boolean = false;
  @ViewChild('chart') chartRef: any;
  token = localStorage.getItem('gItaOkn');
  isLoading : boolean = false;

  constructor(private router: Router, private http: HttpClient, public fb: FormBuilder, private _sharedService: SharedService,private toastr : ToastrService) {
  }

  ngOnInit(): void {
    if(this.token === undefined || this.token === null) {
      this.toastr.warning("No access token given !");
      setTimeout(() => {
        this.router.navigate(['/']);
      }, 1000);
    }
    this.token = this._sharedService.decrypt(localStorage.getItem('gItaOkn')); 
    this.searchFormat();
    this.projectId = this._sharedService.projectId;
    this.projectName = this._sharedService.projectName;

    if (this.projectId === undefined && this.projectName === undefined) {
      let timerInterval : any;
      Swal.fire({
        title: 'Oops... l\'identifiant du projet a été perdu',
        heightAuto: false,
        html: 'Vous allez être redirigé vers la page principale dans <b></b> millisecondes.',
        timer: 5000,
        timerProgressBar: true,
        didOpen: () => {
          Swal.showLoading()
          const b : any = Swal.getHtmlContainer()!.querySelector('b') 
          timerInterval = setInterval(() => {
            b.textContent = Swal.getTimerLeft()
          }, 100)
        },
        willClose: () => {
          clearInterval(timerInterval)
        }
      }).then((result) => {
        if (result.dismiss === Swal.DismissReason.timer) {
          this.router.navigate(['/home']);
        }
      })
    }

    setTimeout(() => {
      this.getCommits("all", new Date().toISOString().split('T')[0], null);
    }, 1000);

    // get all branches
    this.getBranches(this.projectId);
    this.toastr.info("By default, the commits of the day are displayed !");
  }

  searchFormat() {
    this.searchForm = this.fb.group({
      startDate: ['', [Validators.required]],
      endDate: ['', [Validators.required]],
      branch: [''],
    })
  }

  getBranches(projectId: string) {
    let url = `https://gitlab.com/api/v4/projects/${projectId}/repository/branches?access_token=${this.token}&visibility=private&owned=false&per_page=50`;
    this.http.get(url)
      .subscribe(response => {
        this.branches.push(response);
        //this.branches = Array.from(new Set(this.branches[0].map((a: any) => a.commit.author_email))).map(author => { return this.branches[0].find((a: { commit: any }) => a.commit.author_email === author) });
        this.branches = this.branches[0];
      }, error => {
        if(error.status === 401) {
          this.toastr.error("Invalid access token !");
          localStorage.removeItem('gItaOkn');
          this.router.navigate(['/']);
        } else {
          this.toastr.error("Unknown error !");
        }
      });
  }

  onChange() {
    this.isLoading = true;
    this.commits = [];
    this.results = [];
    this.categories = [];
    this.series = [];    
    const { startDate, endDate, branch } = this.searchForm.value;
    if(endDate !== null && startDate !== null) {
      if (this.chartRef) {
        this.chartRef.ngOnDestroy();
        this.highcharts = Highcharts;
      }
      this.getCommits(branch, startDate, endDate);
    }
  }

  onSubmit() {
    this.isLoading = true;
    this.commits = [];
    this.results = [];
    this.categories = [];
    this.series = [];
    if (this.chartRef) {
      this.chartRef.ngOnDestroy();
      this.highcharts = Highcharts;
    }
    let { startDate, endDate, branch } = this.searchForm.value;
    if(branch.length <= 0) branch = "all";

    this.getCommits(branch, startDate, endDate);
  }

  // Get commits from the parameter of the filter method
  async getCommits(branch: any, startDate: any, endDate: any) {
    let url = null;
    if (endDate !== null) {
      url = `https://gitlab.com/api/v4/projects/${this.projectId}/repository/commits` +
        `?access_token=${this.token}&visibility=private&owned=false` +
        `&since=${startDate}T00:00:01&until=${endDate}T23:59:59&per_page=1000&all=true`;
    } else {
      url = `https://gitlab.com/api/v4/projects/${this.projectId}/repository/commits` +
        `?access_token=${this.token}&visibility=private&owned=false` +
        `&since=${startDate}T00:00:01&per_page=1000&all=true`;
    }


    this.http.get(url)
      .subscribe(response => {
        this.commits.push(response);
        this.commits = this.commits[0];
        this.organizeCommits(branch);
      }, error => {
        this.isLoading = false;
        if(error.status === 401) {
          this.toastr.error("Invalid access token !");
          localStorage.removeItem('gItaOkn');
          this.router.navigate(['/']);
        } else {
          this.toastr.error("Unknown error !");
        }
      });
  }


  // Organize commits into objects for the chart
  async organizeCommits(branch: any) {
    if (branch === "all") {
      this.branches.map(branchName => {
        let comm: {
          [key: string]: string
        } = {
          authorEmail: branchName.commit.author_email,
          authorName: branchName.commit.author_name
        };
        this.commits.map(commit => {
          if (branchName.commit.author_email === commit.author_email && !commit.title.includes("Merge")) {
            if (comm.hasOwnProperty(commit.committed_date.split("T")[0])) {
              comm[commit.committed_date.split("T")[0]] = (parseInt(comm[commit.committed_date.split("T")[0]]) + 1).toString();
            } else {
              comm[commit.committed_date.split("T")[0]] = "1";
            }
          }
        });
        this.results.push(comm);
      });
    } else {
      let comm: {
        [key: string]: string
      } = {
        authorEmail: branch.commit.author_email,
        authorName: branch.commit.author_name
      };
      this.commits.map(commit => {
        if (branch.commit.author_email === commit.author_email && !commit.title.includes("Merge")) {
          if (comm.hasOwnProperty(commit.committed_date.split("T")[0])) {
            comm[commit.committed_date.split("T")[0]] = (parseInt(comm[commit.committed_date.split("T")[0]]) + 1).toString();
          } else {
            comm[commit.committed_date.split("T")[0]] = "1";
          }
        }
      });
      this.results.push(comm);
    }
    
    let res = Array.from(new Set(this.results.map(a => a.authorEmail))).map(author => { return this.results.find(a => a.authorEmail === author) });
    
    if (res.length > 0) {
      this.showChart = true;
      await this.displayChart(res);
    }
  }

  displayChart(res: Array<{}>) {
    return new Promise(resolve => {
      // Active branches for this period
      let activeBranches: Array<any> = [];
      let type : string ;
      res.map(a => {
        if (Object.keys(a).length > 2) {
          activeBranches.push(a);
        }
      });

      if(activeBranches.length === 1) {
        type = 'line';
      } else {
        type = 'column';
      }
      // Categories
      activeBranches.map(branch => {
        Object.keys(branch).map(key => {
          if (key !== "authorName" && key !== "authorEmail" && !this.categories.includes(key)) {
            this.categories.push(key);
          }
        });
      });
      this.categories = this.categories.sort();


      // Series
      activeBranches.map(branch => {
        let data: any = [];
        this.categories.map(category => {
          if (Object.keys(branch).includes(category)) {
            data.push(parseInt(branch[category]));
          } else {
            data.push(0);
          }
        });
        this.series.push({
          name: branch.authorName,
          data: data
        });
      });

      if(this.categories.length <= 0 && this.series.length <= 0) {
        this.toastr.info("No commits were made during this period !");
      }

      this.chartOptions = {};

      this.isLoading = false;

      // Highcharts options
      this.chartOptions = {
        accessibility: {
          announceNewData: {
            enabled: true
          }
        },
        credits: {
          enabled: false
        },
        chart: {
          type: type
        },
        title: {
          text: 'Weekporting'
        },
        subtitle: {
          text: this.projectName,
          style: {
            color: '#FF8063'
          }
        },
        xAxis: {
          categories: this.categories,
          crosshair: true
        },
        yAxis: {
          min: 0,
          title: {
            text: 'Nombre de commits'
          }
        },
        tooltip: {
          headerFormat: '<span style = "font-size:10px">{point.key}</span><table>',
          pointFormat: '<tr><td style = "color:{series.color};padding:0">{series.name}: </td>' +
            '<td style = "padding:0"><b>{point.y}</b></td></tr>', footerFormat: '</table>', shared: true, useHTML: true
        },
        plotOptions: {
          column: {
            pointPadding: 0.2,
            borderWidth: 0
          }
        },
        exporting: {
          filename: "weekporting_file",
          buttons: {
            contextButton: {
              menuItems: [
                'viewFullscreen', 'separator', 'downloadPNG',
                'downloadSVG', 'downloadPDF', 'separator', 'downloadXLS'
              ]
            },
          },
          enabled: true,
        },
        navigation: {
          buttonOptions: {
            align: 'right',
            verticalAlign: 'top',
            y: 0
          }
        },
        series: this.series
      }
    })
  }
}

