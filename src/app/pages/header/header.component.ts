import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router,private toastr : ToastrService) { }

  @Input()
  isTokenValid : boolean ;

  ngOnInit(): void {

  }

  logout() {
    localStorage.removeItem('gItaOkn');
    this.toastr.success("Token has been removed !");
    setTimeout(() => {
      this.router.navigate(['/']);
    },2000);
  }

}
