import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {

  @Input()
  project !: any;

  constructor(private _sharedService: SharedService, private router : Router) { }

  ngOnInit(): void {
  }

  goToReporting(){
    this._sharedService.projectId = this.project.id;
    this._sharedService.projectName = this.project.name;
    this.router.navigate(['/reportings']);
  }
}
