import { Component } from '@angular/core';
import "tailwindcss/tailwind.css";
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'weekporting';
  token = localStorage.getItem('gItaOkn');

  constructor(private router : Router, private toastr : ToastrService) {}

  ngOnInit() {
    if(this.token === undefined || this.token === null) {
      this.router.navigate(['/']);
    } else {
      this.router.navigate(['/home']);
    }
  }

  timeout() {
    localStorage.removeItem('gItaOkn');
    setTimeout(() => {
      this.router.navigate(['/']);
    }, 2000);
  }
}
